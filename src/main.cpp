#include "../include/main.hpp"
#include "../extern/beatsaber-hook/shared/customui/customui.hpp"
rapidjson::Document& config_doc = Configuration::config;
CustomUI::TextObject Clock;

bool america = true;
bool show_seconds = true;
float _npos = 0;
float _3pos = 0;

std::string getTime() { //i actually tried to make this _not_ run every frame, but ... nope, guess not. oh well. sucks to be performance.
    time_t now;
    struct tm *now_tm;
    int hour;
    int min;
    int sec;
    now = time(NULL);
    now_tm = localtime(&now);
    hour = now_tm->tm_hour;
    min = now_tm->tm_min;
    sec = now_tm->tm_sec;
    std::string p2 = std::to_string(min);
    std::string p4 = std::to_string(sec);
    std::string amurica;
    if (!america) {
        if (hour > 12) {
            hour = hour - 12;
            amurica = " PM";
        } else {
            amurica = " AM";
        }
    }
    std::string p1 = std::to_string(hour);
    if (hour < 10) {
        p1.insert(0, "0");
    }
    if (min < 10) {
        p2.insert(0, "0");
    }
    if (sec < 10) {
        p4.insert(0, "0");
    }
    std::string p3 = p1 + ":" + p2 + (show_seconds ? ":" + p4 : "") + (!america ? amurica : "");
    return p3;
}

MAKE_HOOK_OFFSETLESS(Thing, void, Il2CppObject* self) {
    Il2CppObject* levelName = il2cpp_utils::GetFieldValue(self, "_scoreText");
    Il2CppObject* levelNameTransform;
    Il2CppObject* levelNameParent;
    il2cpp_utils::RunMethod(&levelNameTransform, levelName, "get_transform");
    il2cpp_utils::RunMethod(&levelNameParent, levelNameTransform, "GetParent");
    Clock.text = "13:37";
    Clock.fontSize = 12.0F;
    Clock.parentTransform = levelNameParent;
    Clock.create();
    Thing(self);
}

MAKE_HOOK_OFFSETLESS(Update, void, Il2CppObject* self) {
    std::string lol = getTime();
    il2cpp_utils::RunMethod(Clock.textMesh, "set_text", il2cpp_utils::createcsstr(getTime()));
    Update(self);
}

//thank futuremappermydud
MAKE_HOOK_OFFSETLESS(Get360, void, Il2CppObject* self, Il2CppObject* difficultyBeatmap, Il2CppObject* overrideEnvironmentSettings, Il2CppObject* overrideColorScheme, Il2CppObject* gameplayModifiers, Il2CppObject* playerSpecificSettings, Il2CppObject* practiceSettings, Il2CppString* backButtonText, bool useTestNoteCutSoundEffects) {
	Il2CppObject *beatmapDataObj;
	int spawnRotationEventsCount;
    il2cpp_utils::RunMethod(&beatmapDataObj, difficultyBeatmap, "get_beatmapData");
	il2cpp_utils::RunMethod(&spawnRotationEventsCount, beatmapDataObj, "get_spawnRotationEventsCount");
    if (spawnRotationEventsCount > 0) {
        log(DEBUG, "360 = true");
        Clock.anchoredPosition = {35.0,(float)40.0+_3pos};
	} else {
        log(DEBUG, "360 = false");
        Clock.anchoredPosition = {175.0,(float)100.0+_npos};
    }
    Get360(self, difficultyBeatmap, overrideEnvironmentSettings, overrideColorScheme, gameplayModifiers, playerSpecificSettings, practiceSettings, backButtonText, useTestNoteCutSoundEffects);
}

extern "C" void load() {
    log(INFO, "clock!");
    Configuration::Load();
    bool foundEverything = true;
    if(config_doc.HasMember("24hr") && config_doc["24hr"].IsBool()){
        america = config_doc["24hr"].GetBool();    
    }else{
        foundEverything = false;
    }
    if(config_doc.HasMember("showseconds") && config_doc["showseconds"].IsBool()){
        show_seconds = config_doc["showseconds"].GetBool();    
    }else{
        foundEverything = false;
    }
    if(config_doc.HasMember("360ypos") && config_doc["360ypos"].IsFloat()){
        _3pos = config_doc["360ypos"].GetFloat();    
    }else{
        foundEverything = false;
    }
    if(config_doc.HasMember("normalypos") && config_doc["normalypos"].IsFloat()){
        _npos = config_doc["normalypos"].GetFloat();    
    }else{
        foundEverything = false;
    }
    if (!foundEverything) {
        config_doc.RemoveAllMembers();
        config_doc.SetObject();
        rapidjson::Document::AllocatorType& allocator = config_doc.GetAllocator();
        config_doc.AddMember("24hr", america, allocator);
        config_doc.AddMember("showseconds", show_seconds, allocator);
        config_doc.AddMember("360ypos", _3pos, allocator);
        config_doc.AddMember("normalypos", _npos, allocator);
        Configuration::Write();
    }
    log(INFO, "clock hooks...");
    INSTALL_HOOK_OFFSETLESS(Thing, il2cpp_utils::FindMethodUnsafe("", "ScoreUIController", "Start", 0));
    INSTALL_HOOK_OFFSETLESS(Update, il2cpp_utils::FindMethodUnsafe("", "SongProgressUIController", "Update", 0));
    INSTALL_HOOK_OFFSETLESS(Get360, il2cpp_utils::FindMethodUnsafe("", "StandardLevelScenesTransitionSetupDataSO", "Init", 8)); 
    log(INFO, "clock hooks!");
}